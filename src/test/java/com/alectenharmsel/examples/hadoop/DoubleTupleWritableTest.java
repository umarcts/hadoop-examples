package com.alectenharmsel.examples.hadoop;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DoubleTupleWritableTest {

    private DoubleTupleWritable lhs;
    private DoubleTupleWritable rhs;

    @Before
    public void setUp() {
        lhs = new DoubleTupleWritable(2, 3);
        rhs = new DoubleTupleWritable(2, 3);
    }

    @Test
    public void testFirst() {
        Assert.assertEquals(2.0, lhs.first, DoubleTupleWritable.epsilon);
    }

    @Test
    public void testSecond() {
        Assert.assertEquals(3.0, lhs.second, DoubleTupleWritable.epsilon);
    }

    @Test
    public void testEquality() {
        Assert.assertEquals(lhs, rhs);
    }

    @Test
    public void testInequality() {
        lhs.first = 2.1;
        Assert.assertNotEquals(lhs, rhs);
    }
}
