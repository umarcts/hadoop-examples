package com.alectenharmsel.examples.hadoop;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.apache.hadoop.mrunit.types.Pair;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AverageNGramLengthTest {

    private MapDriver mapDriver;
    private ReduceDriver reduceDriver;
    private ArrayList<Text> mapInput;
    private DoubleTupleWritable mapOutputs[];

    @Before
    public void setUp() {
        mapDriver = new MapDriver(new AverageNGramLength.Map());
        reduceDriver = new ReduceDriver(new AverageNGramLength.Reduce());
        DoubleTupleWritable daw;

        mapInput = new ArrayList<Text>();
        mapInput.add(new Text("A'que_ADJ\t1900\t4\t4"));
        mapInput.add(new Text("A.J.B._NOUN\t1900\t1\t1"));
        mapInput.add(new Text("A.N._NOUN\t1900\t17\t10"));
        mapInput.add(new Text("A.o.\t1900\t4\t3"));
        mapInput.add(new Text("A116e\t1900\t5\t5"));
        mapInput.add(new Text("A17\t1900\t5\t5"));
        mapInput.add(new Text("A1C13_.\t1900\t1\t1"));
        mapInput.add(new Text("ABOT_VERB\t1900\t1\t1"));
        mapInput.add(new Text("ABUBEKR\t1900\t3\t3"));
        mapInput.add(new Text("ABW\t1900\t1\t1"));

        mapOutputs = new DoubleTupleWritable[mapInput.size()];
        mapOutputs[0] = new DoubleTupleWritable(9.0, 4.0);
        mapOutputs[1] = new DoubleTupleWritable(11.0, 1.0);
        mapOutputs[2] = new DoubleTupleWritable(9, 17);
        mapOutputs[3] = new DoubleTupleWritable(4, 4);
        mapOutputs[4] = new DoubleTupleWritable(5, 5);
        mapOutputs[5] = new DoubleTupleWritable(3, 5);
        mapOutputs[6] = new DoubleTupleWritable(7, 1);
        mapOutputs[7] = new DoubleTupleWritable(9, 1);
        mapOutputs[8] = new DoubleTupleWritable(7, 3);
        mapOutputs[9] = new DoubleTupleWritable(3, 1);
    }

    @Test
    public void testMapFirst() throws IOException {
        mapDriver.withInput(new LongWritable(0), mapInput.get(0))
            .withOutput(new IntWritable(1900), mapOutputs[0])
            .runTest();
    }

    @Test
    public void testMapSecond() throws IOException {
        mapDriver.withInput(new LongWritable(0), mapInput.get(1))
            .withOutput(new IntWritable(1900), mapOutputs[1])
            .runTest();
    }

    @Test
    public void testMapThird() throws IOException {
        mapDriver.withInput(new LongWritable(0), mapInput.get(2))
            .withOutput(new IntWritable(1900), mapOutputs[2])
            .runTest();
    }

    @Test
    public void testMapFourth() throws IOException {
        mapDriver.withInput(new LongWritable(0), mapInput.get(3))
            .withOutput(new IntWritable(1900), mapOutputs[3])
            .runTest();
    }

    @Test
    public void testMapFifth() throws IOException {
        mapDriver.withInput(new LongWritable(0), mapInput.get(4))
            .withOutput(new IntWritable(1900), mapOutputs[4])
            .runTest();
    }

    @Test
    public void testMapSixth() throws IOException {
        mapDriver.withInput(new LongWritable(0), mapInput.get(5))
            .withOutput(new IntWritable(1900), mapOutputs[5])
            .runTest();
    }

    @Test
    public void testMapSeventh() throws IOException {
        mapDriver.withInput(new LongWritable(0), mapInput.get(6))
            .withOutput(new IntWritable(1900), mapOutputs[6])
            .runTest();
    }

    @Test
    public void testMapEighth() throws IOException {
        mapDriver.withInput(new LongWritable(0), mapInput.get(7))
            .withOutput(new IntWritable(1900), mapOutputs[7])
            .runTest();
    }

    @Test
    public void testMapNinth() throws IOException {
        mapDriver.withInput(new LongWritable(0), mapInput.get(8))
            .withOutput(new IntWritable(1900), mapOutputs[8])
            .runTest();
    }

    @Test
    public void testMapTenth() throws IOException {
        mapDriver.withInput(new LongWritable(0), mapInput.get(9))
            .withOutput(new IntWritable(1900), mapOutputs[9])
            .runTest();
    }

    @Test
    public void testReduce() throws IOException {
        DoubleTupleWritable answer = new DoubleTupleWritable(7.047619, 42);
        List<Pair<IntWritable, DoubleTupleWritable> > res;
        ArrayList<DoubleTupleWritable> redInputs =
            new ArrayList<DoubleTupleWritable>();

        for (int i = 0; i < mapOutputs.length; i++) {
            redInputs.add(mapOutputs[i]);
        }

        res = reduceDriver.withInput(new IntWritable(1900), redInputs)
            .run();
        Assert.assertEquals(1900, res.get(0).getFirst().get());
        Assert.assertEquals(answer, res.get(0).getSecond());
    }
}
