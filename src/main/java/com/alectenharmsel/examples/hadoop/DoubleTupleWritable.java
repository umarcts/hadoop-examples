package com.alectenharmsel.examples.hadoop;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.hadoop.io.Writable;

public class DoubleTupleWritable implements Writable {
    public double first;
    public double second;
    public static final double epsilon = 0.001;

    public DoubleTupleWritable() {
        first = 0.0;
        second = 0.0;
    }

    public DoubleTupleWritable(double first, double second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public String toString() {
        return Double.toString(first) + "," + Double.toString(second);
    }

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeDouble(first);
        out.writeDouble(second);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        first = in.readDouble();
        second = in.readDouble();
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object rhs) {
        if (!(rhs instanceof DoubleTupleWritable)) {
            return false;
        }

        DoubleTupleWritable tmpRHS = (DoubleTupleWritable) rhs;

        double f = Math.abs(first / tmpRHS.first) - 1.0;
        double s = Math.abs(second / tmpRHS.second) - 1.0;

        if (f > epsilon || s > epsilon) {
            return false;
        }

        return true;
    }
}

