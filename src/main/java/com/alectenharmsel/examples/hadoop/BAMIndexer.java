/* 
 * Copyright 2015 Alec Ten Harmsel
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.alectenharmsel.examples.hadoop;

import java.io.IOException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.seqdoop.hadoop_bam.SplittingBAMIndexer;

/**
 * Indexes BAM files for use with BAMInputFormat.
 */
public class BAMIndexer extends Configured implements Tool
{
    public int run(String[] args) throws Exception
    {
        Configuration conf = getConf();
        conf.set("input", args[0]);

        try
        {
            SplittingBAMIndexer.run(conf);
        }
        catch (IOException e)
        {
            return 1;
        }

        return 0;
    }

    public static void main(String args[]) throws Exception
    {
        GenericOptionsParser parse = new GenericOptionsParser(
                        new Configuration(), args);
        Configuration conf = parse.getConfiguration();

        int res = ToolRunner.run(conf, new BAMIndexer(),
                        parse.getRemainingArgs());

        System.exit(res);
    }
}
