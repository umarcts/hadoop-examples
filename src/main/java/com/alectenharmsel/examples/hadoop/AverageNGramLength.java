/* 
 * Copyright 2015 Alec Ten Harmsel
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.alectenharmsel.examples.hadoop;

import java.io.IOException;
import java.util.ArrayList;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

public class AverageNGramLength
{
    public static class Map extends Mapper<LongWritable, Text, IntWritable, DoubleTupleWritable>
    {
        public void map(LongWritable key, Text contents, Context context) throws IOException, InterruptedException
        {
            String[] arr = contents.toString().split("\t");
            IntWritable year;
            DoubleTupleWritable avg;

            try {
                year = new IntWritable(Integer.parseInt(arr[1]));
            } catch (NumberFormatException e) {
                return;
            }

            try {
                double len = arr[0].length();
                double n = Double.parseDouble(arr[2]);
                avg = new DoubleTupleWritable(len, n);
            } catch (NumberFormatException e) {
                return;
            }

            context.write(year, avg);
        }
    }

    public static class Reduce extends Reducer<IntWritable, DoubleTupleWritable, IntWritable, DoubleTupleWritable>
    {
        public void reduce(IntWritable year, Iterable<DoubleTupleWritable> means, Context context) throws IOException, InterruptedException
        {
            double len = 0.0;
            double n = 0.0;
            DoubleTupleWritable out;

            for (DoubleTupleWritable mean : means) {
                double tmplen = mean.first;
                double tmpn = mean.second;
                len += tmplen * tmpn;
                n += tmpn;
            }

            out = new DoubleTupleWritable(len / n, n);
            context.write(year, out);
        }
    }

    public static void main(String[] args) throws Exception
    {
        GenericOptionsParser parser = new GenericOptionsParser(new Configuration(), args);
        Configuration conf = parser.getConfiguration();
        conf.set("mapreduce.output.textoutputformat.separator", ",");

        String[] remainingArgs = parser.getRemainingArgs();
        if(remainingArgs.length != 2)
        {
            System.err.println("Usage: AverageNGramLength <input> <output>");
            System.exit(-1);
        }

        Job job = Job.getInstance(conf, "AverageNGramLength");
        job.setJarByClass(AverageNGramLength.class);

        job.setMapperClass(Map.class);
        job.setCombinerClass(Reduce.class);
        job.setReducerClass(Reduce.class);

        job.setOutputKeyClass(IntWritable.class);
        job.setOutputValueClass(DoubleTupleWritable.class);

        FileInputFormat.addInputPath(job, new Path(remainingArgs[0]));
        FileOutputFormat.setOutputPath(job, new Path(remainingArgs[1]));

        job.submit();
        System.exit(0);
    }
}
