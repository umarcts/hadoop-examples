# Hadoop Examples
This repository contains some example Hadoop jobs to demonstrate how to test
and package them. In the words of Kent Beck:

> If a feature doesn't have a test, it doesn't exist

To build the code, simply run:

    git clone https://bitbucket.org/umarcts/hadoop-examples.git
    cd hadoop-examples
    ./gradlew jar

The JARs will be in `build/libs`. The JAR that ends in `-all.jar` has
dependencies necessary for certain jobs bundled in it for submission to
a Hadoop cluster.

If you have Java 7 or above, the code can be run through FindBugs and PMD:

    ./gradlew build

Reports will be generated into `build/reports`.

## AverageNGramLength
Reads in a Google NGrams 1-grams text file and produces year averages of word
length.

## BAMIndexer
Does HDFS-specific indexing of BAM files.

## BAMSummary
Computes summary statistics, similar to `samtools -flagstat`. BAMIndexer should
be run on every BAM that BAMSummary runs on for performance.

## CodeTokenizer
CodeTokenizer reads input source code, tokenizes it, and reports the counts of
all the tokens.

## MoabLicenseInfo
MoabLicenseInfo can scan Moab (an HPC scheduler from Adaptive Computing) logs
to determine software license usage statistics.
